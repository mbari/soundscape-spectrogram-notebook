FROM jupyter/base-notebook

MAINTAINER Danelle Cline <dcline@mbari.org>

USER root
RUN apt-get -qq update && \
    apt-get install -y libsndfile1 && \
    apt-get install -y gcc && \
    apt-get install -y libsm6 && \
    apt-get install -y libatlas-base-dev && \
    apt-get install -y libxrender-dev && \
    apt-get install -y libxext-dev && \
    apt-get install -y python3-tables


#########################################################################
# Uncomment to include PDF export support (makes Docker images larger!)
#RUN apt-get install -y texlive-xetex pandoc
#########################################################################
RUN apt-get install -y libgtk2.0-dev

USER $NB_UID

RUN pip install Cython numpy
ADD requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt

WORKDIR /home/jovyan/work
ADD src .